import React from 'react'
import {observer} from 'mobx-react-lite'

import appState from '../../shared/store/appState'
import Canvas from '../../entities/canvas'
import ToolControls from '../../widgets/tool-controls'
import ModeMenu from '../../widgets/mode-menu'
import LobbyModal from '../../features/modals/lobby-modal'
import PanelSide from '../../shared/UI/panel-side'
import Plug from '../../shared/UI/plug'

const Main = observer(() => {
    return (
        <>
            <ToolControls>
                <Canvas/>
                <PanelSide right>
                    <Plug/>
                    <ModeMenu/>
                </PanelSide>
            </ToolControls>
            {appState.isOpenLobbyModal && <LobbyModal/>}
        </>
    )
})

export default Main
