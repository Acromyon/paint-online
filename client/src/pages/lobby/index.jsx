import React from 'react'
import {observer} from 'mobx-react-lite'

import userState from '../../shared/store/userState'
import ToolControls from '../../widgets/tool-controls'
import Chat from '../../widgets/chat'
import ModeMenu from '../../widgets/mode-menu'
import Canvas from '../../entities/canvas'
import LoginModal from '../../features/modals/login-modal'
import PanelSide from '../../shared/UI/panel-side'
import Separator from '../../shared/UI/separator'

import styles from './styles.module.scss'

const Lobby = observer(() => {
    return (
        <>
            <ToolControls>
                <Canvas/>
                <PanelSide right>
                    <Chat/>
                    <Separator className={styles.separator}/>
                    <ModeMenu/>
                </PanelSide>
            </ToolControls>
            {!userState.isLogged && <LoginModal/>}
        </>
    )
})

export default Lobby
