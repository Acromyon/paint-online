import React from 'react'

import ChatLine from '../../features/chat-line'
import ChatControl from '../../features/chat-control'

const Chat = () => {
    return (
        <>
            <ChatLine/>
            <ChatControl/>
        </>
    )
}

export default Chat
