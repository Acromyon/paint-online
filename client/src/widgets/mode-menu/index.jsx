import React from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import {observer} from 'mobx-react-lite'

import appState from '../../shared/store/appState'
import userState from '../../shared/store/userState'
import chatState from '../../shared/store/chatState'
import Button from '../../shared/UI/button'
import {Chunk, Text} from '../../shared/UI/typography'

import styles from './styles.module.scss'

const ModeMenu = observer(() => {
    const navigate = useNavigate()
    const {roomId} = useParams()
    const isLogged = userState.isLogged
    
    const createNew = () => {
        userState.setIsLogged(false)
        userState.WS && userState.disconnect()
        appState.setIsOpenLobbyModal(true)
        chatState.setChatLine([])
        navigate('/')
    }
    
    const copyUrl = () => {
        navigator.clipboard.writeText(window.location.href)
            .then(() => {
                console.log('lobby link coped')
            })
    }
    
    const textContent = isLogged
        ? <>Lobby: <Chunk fw="normal" fz={20}>{roomId}</Chunk></>
        : 'Offline'
    
    return (
        <>
            <Text fw="bold" fz={18} className={styles.title}>
                {textContent}
            </Text>
            <div className={styles.btnContainer}>
                {isLogged && window.isSecureContext && (
                    <Button
                        className={styles.btn}
                        type="primary"
                        onClick={copyUrl}
                    >
                        Copy URL
                    </Button>
                )}
                <Button
                    className={styles.btn}
                    type="primary"
                    onClick={createNew}
                >
                    Create new
                </Button>
            </div>
        </>
    )
})

export default ModeMenu
