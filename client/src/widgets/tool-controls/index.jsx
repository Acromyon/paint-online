import React from 'react'

import PanelVertical from '../../shared/UI/panel-vertical'
import Toolbar from '../../entities/toolbar'
import PanelSide from '../../shared/UI/panel-side'
import TuningBar from '../../entities/tuning-bar'

import styles from './styles.module.scss'

const ToolControls = ({children}) => {
    return (
        <>
            <PanelVertical>
                <Toolbar/>
            </PanelVertical>
            <div className={styles.contentWrap}>
                <PanelSide>
                    <TuningBar/>
                </PanelSide>
                {children}
            </div>
        </>
    )
}

export default ToolControls
