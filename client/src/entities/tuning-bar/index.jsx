import React from 'react'
import cn from 'classnames'
import {observer} from 'mobx-react-lite'

import toolState from '../../shared/store/toolState'
import {toolTypeMap} from '../../shared/constants/maps'

import styles from './styles.module.scss'

const TuningBar = observer(() => {
    const toolType = toolState.tool?.toString()
    const controls = {
        stroke: true,
        fill: true,
        lineWidth: true,
    }
    
    switch (toolType) {
        case toolTypeMap.BRUSH:
            controls.fill = false
            break
        case toolTypeMap.ERASER:
            controls.stroke = false
            controls.fill = false
            break
        case toolTypeMap.LINE:
            controls.fill = false
            break
        default:
            console.log('unknowing tool', toolType)
    }
    
    const changeStrokeColor = (e) => {
        toolState.setStrokeColor(e.target.value)
    }
    
    const changeFillColor = (e) => {
        toolState.setFillColor(e.target.value)
    }
    
    const changeWidth = (e) => {
        toolState.setLineWidth(e.target.value)
    }
    
    return (
        <>
            <h3 className={styles.controls}>Controls</h3>
            {controls.stroke && (
                <label className={styles.label}>
                    <span>
                        Stroke:
                    </span>
                    <input
                        onChange={changeStrokeColor}
                        className={cn(styles.input, styles.btn, styles.colorPickBtn)}
                        type="color"
                    />
                </label>
            )}
            
            {controls.fill && (
                <label className={styles.label}>
                    <span>
                        Fill:
                    </span>
                    <input
                        onChange={changeFillColor}
                        className={cn(styles.input, styles.btn, styles.colorPickBtn)}
                        type="color"
                    />
                </label>
            )}
            
            {controls.lineWidth && (
                <label className={styles.label}>
                    <span>
                        Line Width:
                    </span>
                    <input
                        onChange={changeWidth}
                        className={styles.input}
                        type="number"
                        defaultValue={1}
                        min={1}
                        max={50}
                    />
                </label>
            )}
        </>
    )
})

export default TuningBar
