import React from 'react'
import {observer} from 'mobx-react-lite'
import cn from 'classnames'

import appState from '../../shared/store/appState'
import toolState from '../../shared/store/toolState'
import canvasState from '../../shared/store/canvasState'
import userState from '../../shared/store/userState'
import Brush from '../../features/tools/figures/Brush'
import Rect from '../../features/tools/figures/Rect'
import Circle from '../../features/tools/figures/Circle'
import Eraser from '../../features/tools/figures/Eraser'
import Line from '../../features/tools/figures/Line'
import CleanUpModal from '../../features/modals/clean-up-modal'
import {shortcutMap} from '../../app/keyboardControl'
import {methodMap} from '../../shared/api/WS'
import {controlMap} from '../../shared/constants/maps'

import styles from './styles.module.scss'

export const undoHandler = () => {
    canvasState.undo()
    
    if (userState.isLogged) {
        const control = {type: controlMap.UNDO}
        userState.sendMessage(methodMap.CONTROL, control)
    }
}

export const redoHandler = () => {
    canvasState.redo()
    
    if (userState.isLogged) {
        const control = {type: controlMap.REDO}
        userState.sendMessage(methodMap.CONTROL, control)
    }
}

const Toolbar = observer(() => {
    return (
        <>
            <button className={cn(styles.btn, styles.brushBtn)} onClick={() => toolState.setTool(new Brush(canvasState.canvas))}/>
            <button className={cn(styles.btn, styles.rectBtn)} onClick={() => toolState.setTool(new Rect(canvasState.canvas))}/>
            <button className={cn(styles.btn, styles.circleBtn)} onClick={() => toolState.setTool(new Circle(canvasState.canvas))}/>
            <button className={cn(styles.btn, styles.eraserBtn)} onClick={() => toolState.setTool(new Eraser(canvasState.canvas))}/>
            <button className={cn(styles.btn, styles.lineBtn)} onClick={() => toolState.setTool(new Line(canvasState.canvas))}/>
            <button
                className={cn(styles.btn, styles.undoBtn)}
                title={shortcutMap.undo}
                onClick={undoHandler}
                disabled={!Boolean(canvasState.undoList.length)}
            />
            <button
                className={cn(styles.btn, styles.redoBtn)}
                title={shortcutMap.redo}
                onClick={redoHandler}
                disabled={!Boolean(canvasState.redoList.length)}
            />
            <button className={cn(styles.btn, styles.saveBtn)} title={shortcutMap.save} onClick={() => canvasState.saveImageToLocal()}/>
            <button className={cn(styles.btn, styles.deleteBtn)} title={shortcutMap.cleanUp} onClick={() => appState.setIsOpenCleanUpModal(true)}/>
            {appState.isOpenCleanUpModal && <CleanUpModal/>}
        </>
    )
})

export default Toolbar
