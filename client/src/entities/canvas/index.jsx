import React, {useEffect, useRef} from 'react'
import {observer} from 'mobx-react-lite'

import canvasState from '../../shared/store/canvasState'
import userState from '../../shared/store/userState'
import toolState from '../../shared/store/toolState'
import Brush from '../../features/tools/figures/Brush'

import styles from './styles.module.scss'

const Canvas = observer(() => {
    const isLogged = userState.isLogged
    const canvasRef = useRef()
    
    useEffect(() => {
        canvasState.setCanvas(canvasRef.current)
        toolState.setTool(new Brush(canvasState.canvas))
    }, [])
    
    useEffect(() => {
        if (isLogged) {
            canvasState.syncFullImage(userState.roomId)
        }
    }, [isLogged])
    
    return (
        <div className={styles.canvasWrap}>
            <canvas
                ref={canvasRef}
                className={styles.canvas}
                width={900}
                height={600}
            />
        </div>
    )
})

export default Canvas
