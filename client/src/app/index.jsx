import React from 'react'
import {BrowserRouter, Routes, Route, Navigate} from 'react-router-dom'

import KeyboardProvider from './keyboardControl'

import Main from '../pages/main'
import Lobby from '../pages/lobby'

import styles from './styles.module.scss'

function App() {
    return (
        <KeyboardProvider>
            <BrowserRouter>
                <div className={styles.appWrap}>
                    <Routes>
                        <Route
                            index
                            element={<Main/>}
                        />
                        <Route
                            path="/lobby/:roomId"
                            element={<Lobby/>}
                        />
                        <Route
                            path="*"
                            element={<Navigate to="/"/>}
                        />
                    </Routes>
                </div>
            </BrowserRouter>
        </KeyboardProvider>
    )
}

export default App
