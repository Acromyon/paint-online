import {useEffect} from 'react'

import {checkPressedKey} from '../shared/utils/keyboardControl'
import canvasState from '../shared/store/canvasState'
import appState from '../shared/store/appState'
import {redoHandler, undoHandler} from '../entities/toolbar'

const keyboardComboSchem = (e) => {
    if (e.ctrlKey && !e.shiftKey) {
        checkPressedKey(e,'KeyZ', undoHandler)
    }
    if (e.ctrlKey && e.shiftKey) {
        checkPressedKey(e,'KeyZ', redoHandler)
        checkPressedKey(e,'KeyS', () => canvasState.saveImageToLocal())
    }
}

const keyboardSchem = (e) => {
    checkPressedKey(e,'Delete', () => appState.setIsOpenCleanUpModal(true))
}

const KeyboardProvider = ({children}) => {
    useEffect(() => {
        document.body.addEventListener('keypress', keyboardComboSchem) // only "keypress" catches modifies (ctrl, shift, alt)
        document.body.addEventListener('keyup', keyboardSchem) // Del, Backspace, etc. not works with "keypress"
        
        return () => {
            document.body.removeEventListener('keypress', keyboardComboSchem)
            document.body.addEventListener('keyup', keyboardSchem)
        }
    }, [])
    
    return children
}

export const shortcutMap = {
    undo: 'ctrl+z',
    redo: 'ctrl+shift+z',
    cleanUp: 'delete',
    save: 'ctrl+shift+s',
    sendChatMsg: 'ctrl+enter',
}

export default KeyboardProvider
