import React from 'react'

import styles from './styles.module.scss'

const PanelVertical = ({ children }) => {
    return (
        <div className={styles.panel}>
            <div className={styles.container}>
                {children}
            </div>
        </div>
    )
}

export default PanelVertical
