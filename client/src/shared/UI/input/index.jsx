import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const Input = ({ className, children, ...props }) => {
    return (
        <input className={cn(styles.input, className)} {...props}>
            {children}
        </input>
    )
}

export default Input
