import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const TextArea = ({ className, textAreaRef, ...props }) => {
    return (
        <div className={cn(styles.textarea, className)} {...props} ref={textAreaRef} contentEditable/>
    )
}

export default TextArea
