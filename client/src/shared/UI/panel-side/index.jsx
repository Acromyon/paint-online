import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const PanelSide = ({ children, right }) => {
    return (
        <div className={cn(styles.panel, right && styles.panelRight )}>
            <div className={styles.container}>
                {children}
            </div>
        </div>
    )
}

export default PanelSide
