import React, {createElement} from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

export const Title = ({ tagType = 'h4', children, ...props }) => createElement(tagType, props, children)

export const Text = ({ fz, fw, className, children, ...props }) => {
    return (
        <p className={cn(styles[`fz${fz}`], styles[fw], className)} {...props}>
            {children}
        </p>
    )
}

export const Chunk = ({ fz, fw, className, children, ...props }) => {
    return (
        <span className={cn(styles[`fz${fz}`], styles[fw], className)} {...props}>
            {children}
        </span>
    )
}
