import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const Button = ({ onClick, type, className, children, ...props }) => {
    return (
        <button
            onClick={onClick}
            className={cn(styles.button, type, className)}
            {...props}
        >
            {children}
        </button>
    )
}

export default Button
