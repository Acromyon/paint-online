import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const Plug = ({className}) => {
    return (
        <div className={cn(styles.plug, className)} />
    )
}

export default Plug
