import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const Modal = ({ close, className, children, ...props }) => {
    return (
        <div className={styles.background} onClick={close}>
            <div
                className={cn(styles.modal, className)}
                onClick={(e) => e.stopPropagation()}
                {...props}
            >
                {children}
            </div>
        </div>
    )
}

export default Modal
