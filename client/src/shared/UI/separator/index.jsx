import React from 'react'
import cn from 'classnames'

import styles from './styles.module.scss'

const Separator = ({className}) => {
    return (
        <hr className={cn(styles.separator, className)} />
    )
}

export default Separator
