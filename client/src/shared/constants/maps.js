export const controlMap = {
    START: 'start',
    FINISH: 'finish',
    UNDO: 'undo',
    REDO: 'redo',
    CLEANUP: 'cleanUp',
}

export const toolTypeMap = {
    BRUSH: '[object Brush]',
    ERASER: '[object Eraser]',
    RECT: '[object Rect]',
    CIRCLE: '[object Circle]',
    LINE: '[object Line]',
    PREVIEW: '[object PreviewDrawing]',
    SEAMLESS: '[object SeamlessDrawing]',
    TOOL: '[object Tool]',
}
