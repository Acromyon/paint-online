import {makeAutoObservable} from 'mobx'

import Brush from '../../features/tools/figures/Brush'
import Rect from '../../features/tools/figures/Rect'
import Eraser from '../../features/tools/figures/Eraser'
import Circle from '../../features/tools/figures/Circle'
import Line from '../../features/tools/figures/Line'
import {controlMap, toolTypeMap} from '../constants/maps'
import {getFullImage, postFullImage} from '../api/http'

class CanvasState {
    canvas = null
    ctx = null
    undoList = []
    redoList = []
    
    constructor() {
        makeAutoObservable(this)
    }
    
    setCanvas(canvas) {
        this.canvas = canvas
        this.ctx = this.canvas.getContext('2d')
    }
    
    drawReceivedFigure(figure) {
        const backupStyles = this.swapStyles(figure.styles)
        
        switch (figure.type) {
            case toolTypeMap.BRUSH:
                Brush.draw(this.ctx, figure.x, figure.y)
                break
            case toolTypeMap.ERASER:
                Eraser.draw(this.ctx, figure.x, figure.y)
                break
            case toolTypeMap.RECT:
                Rect.draw(this.ctx, figure.x, figure.y, figure.w, figure.h)
                break
            case toolTypeMap.CIRCLE:
                Circle.draw(this.ctx, figure.x, figure.y, figure.radius)
                break
            case toolTypeMap.LINE:
                Line.draw(this.ctx, figure.startX, figure.startY, figure.x, figure.y)
                break
            default:
                console.log('unknowing tool')
        }
        
        this.swapStyles(backupStyles)
    }
    
    applyReceivedCommand(control) {
        switch (control.type) {
            case controlMap.START:
                this.pushToUndo()
                this.clearRedo()
                return
            case controlMap.FINISH:
                return this.ctx.beginPath()
            case controlMap.UNDO:
                return this.undo()
            case controlMap.REDO:
                return this.redo()
            case controlMap.CLEANUP:
                return this.cleanUpCanvas()
            default:
                console.log('unknowing control')
        }
    }
    
    swapStyles(styles) {
        const backupStyles = {}
    
        for (const key in styles) {
            backupStyles[key] = this.ctx[key]
            this.ctx[key] = styles[key]
        }
        
        return backupStyles
    }
    
    keepLengthLimit(arr) {
        const arrLimit = 20
        
        if (arr.length > arrLimit) {
            arr.shift()
        }
    }
    
    pushToUndo() {
        this.undoList.push(this.canvas.toDataURL())
        this.keepLengthLimit(this.undoList)
    }
    
    pushToRedo() {
        this.redoList.push(this.canvas.toDataURL())
        this.keepLengthLimit(this.redoList)
    }
    
    clearRedo() {
        this.redoList = []
    }
    
    undo() {
        if (this.undoList.length) {
            const dataUrl = this.undoList.pop()
            this.pushToRedo()
            
            this.setImgToCanvas(dataUrl)
        }
    }
    
    redo() {
        if (this.redoList.length) {
            const dataUrl = this.redoList.pop()
            this.pushToUndo()
            
            this.setImgToCanvas(dataUrl)
        }
    }
    
    cleanUpCanvas() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    }
    
    setImgToCanvas(dataUrl) {
        const img = new Image()
        img.src = dataUrl
        
        img.onload = () => {
            this.cleanUpCanvas()
            this.ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height)
        }
    }
    
    syncFullImage(roomId) {
        getFullImage(roomId)
            .then(res => this.setImgToCanvas(res.data))
        
        this.canvas.addEventListener('mouseup', () => {
            postFullImage(roomId, {img: this.canvas.toDataURL()})
                .then(res => console.log(res.data))
        })
    }
    
    saveImageToLocal() {
        const link = document.createElement('a')
        link.download = 'draft.png'
        link.href = this.canvas.toDataURL()
        link.click()
    }
}

const canvasState = new CanvasState()
export default canvasState
