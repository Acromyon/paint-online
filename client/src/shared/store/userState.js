import {makeAutoObservable} from 'mobx'

import createWSConnection from '../api/WS'

class UserState {
    WS = null
    username = ''
    userId = null
    roomId = null
    isLogged = false
    
    constructor() {
        makeAutoObservable(this)
    }
    
    setUsername(username) {
        this.username = username
    }
    
    setIsLogged(isLogged) {
        this.isLogged = isLogged
    }
    
    setWS (WS) {
        this.WS = WS
    }
    
    setUserId (userId) {
        this.userId = userId
    }
    
    setRoomId (roomId) {
        this.roomId = roomId
    }
    
    sendMessage(method, payload = null, attached = {}) {
        this.WS.send(JSON.stringify({
            roomId: this.roomId,
            userId: this.userId,
            username: this.username,
            method,
            payload,
            ...attached
        }))
        
        console.log(`sent ${method}`, payload)
    }
    
    connectToLobby(roomId, username) {
        const {WS, userId} = createWSConnection(roomId, username)
        
        this.setWS(WS)
        this.setUserId(userId)
        this.setRoomId(roomId)
        this.setUsername(username)
        
        window.addEventListener('beforeunload', this.disconnect.bind(this))
    }
    
    disconnect() {
        this.WS.close(3000, JSON.stringify({
            username: this.username,
            userId: this.userId,
            roomId: this.roomId,
        }))
        
        this.setWS(null)
        this.setUserId(null)
        this.setRoomId(null)
        this.setUsername('')
    }
}

const userState = new UserState()
export default userState
