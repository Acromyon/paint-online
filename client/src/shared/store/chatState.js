import {makeAutoObservable} from 'mobx'

const chatLineLimit = 100

class ChatState {
    chatLine = []
    
    constructor() {
        makeAutoObservable(this)
    }
    
    setChatLine(value) {
        this.chatLine = value
    }
    
    addNewMessage(msg) {
        this.chatLine.push(msg)
        this.checkChatLineLimit()
    }
    
    checkChatLineLimit() {
        if (this.chatLine.length > chatLineLimit) {
            this.chatLine = this.chatLine.slice(25)
        }
    }
}

const chatState = new ChatState()
export default chatState
