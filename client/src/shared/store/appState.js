import {makeAutoObservable} from 'mobx'

class AppState {
    isOpenLobbyModal = true
    isOpenCleanUpModal = false
    
    constructor() {
        makeAutoObservable(this)
    }
    
    setIsOpenLobbyModal(value) {
        this.isOpenLobbyModal = value
    }
    
    setIsOpenCleanUpModal(value) {
        this.isOpenCleanUpModal = value
    }
}

const appState = new AppState()
export default appState
