export const checkPressedKey = (e, expectedKey, callback) => {
    if (e.code === expectedKey) callback()
}

export const checkedPressKeyWithModify = (e, expectedKey, modify, callback) => {
    if (e[modify]) checkPressedKey(e, expectedKey, callback)
}
