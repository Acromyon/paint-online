export const hexadecimalStr = () => `f${(+new Date()).toString(16)}`

export const decimalStr = (length = 2) => Math.floor(Math.random() * Math.pow(10, length)).toString()
