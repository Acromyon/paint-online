import axios from 'axios'

const URL = process.env.REACT_APP_URL
const PORT = process.env.REACT_APP_SERVER_PORT
const PROTOCOL = process.env.REACT_APP_PROTOCOL

export const getFullImage = (roomId) => axios.get(`${PROTOCOL}://${URL}:${PORT}/api/image?id=${roomId}`)
export const postFullImage = (roomId, data) => axios.post(`${PROTOCOL}://${URL}:${PORT}/api/image?id=${roomId}`, data)
