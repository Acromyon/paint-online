import {decimalStr} from '../utils/createUniqueString'
import canvasState from '../store/canvasState'
import chatState from '../store/chatState'

const URL = process.env.REACT_APP_URL
const PORT = process.env.REACT_APP_SERVER_PORT
const WS_PROTOCOL = process.env.REACT_APP_WS_PROTOCOL

const createWSConnection = (roomId, username) => {
    const WS = new WebSocket(`${WS_PROTOCOL}://${URL}:${PORT}`)
    const userId = decimalStr(4)
    
    WS.onopen = () => {
        WS.send(JSON.stringify({
            roomId,
            userId,
            username,
            method: methodMap.CONNECTION,
        }))
    }
    
    WS.onmessage = (e) => {
        const data = JSON.parse(e.data)
        
        switch (data.method) {
            case methodMap.TEXT:
                chatState.addNewMessage(data.payload)
                break
            case methodMap.DRAW:
                canvasState.drawReceivedFigure(data.payload)
                break
            case methodMap.CONTROL:
                canvasState.applyReceivedCommand(data.payload)
                break
            default:
                console.log('unknowing message method', data)
        }
    }
    
    return {
        WS,
        userId
    }
}

export const methodMap = {
    TEXT: 'text',
    DRAW: 'draw',
    CONTROL: 'control',
    CONNECTION: 'connection'
}

export default createWSConnection
