import canvasState from '../../shared/store/canvasState'
import userState from '../../shared/store/userState'
import {methodMap} from '../../shared/api/WS'
import {controlMap, toolTypeMap} from '../../shared/constants/maps'

export default class Tool {
    constructor(canvas) {
        this.canvas = canvas
        this.ctx = canvas.getContext('2d')
        this.listenEvents()
    }
    
    mouseUpHandler
    mouseDownHandler
    mouseMoveHandler
    
    listenEvents() {
        this.canvas.onmouseup = this.mouseUpHandler.bind(this)
        this.canvas.onmousedown = this.mouseDownHandler.bind(this)
        this.canvas.onmousemove = this.mouseMoveHandler.bind(this)
    }
    
    toString() {
        return toolTypeMap.TOOL
    }
    
    set fillColor(color) {
        this.ctx.fillStyle = color
    }
    
    set strokeColor(color) {
        this.ctx.strokeStyle = color
    }
    
    set lineWidth(color) {
        this.ctx.lineWidth = color
    }
    
    beforeStartDrawing() {
        canvasState.pushToUndo()
        canvasState.clearRedo()
        
        if (userState.isLogged) {
            const control = {type: controlMap.START}
            userState.sendMessage(methodMap.CONTROL, control)
        }
    }
}
