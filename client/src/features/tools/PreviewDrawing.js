import Tool from './Tool'
import userState from '../../shared/store/userState'
import canvasState from '../../shared/store/canvasState'
import {methodMap} from '../../shared/api/WS'
import {toolTypeMap} from '../../shared/constants/maps'

export default class PreviewDrawing extends Tool {
    toString() {
        return toolTypeMap.PREVIEW
    }
    
    mouseUpHandler() {
        this.mouseDown = false
        
        if (userState.isLogged) {
            const figure = this.composeFigure()
            userState.sendMessage(methodMap.DRAW, figure)
        }
    }
    
    mouseDownHandler(e) {
        this.beforeStartDrawing()
        
        this.mouseDown = true
        this.prevDraft = this.canvas.toDataURL()
        
        this.startX = e.pageX - e.target.offsetLeft
        this.startY = e.pageY - e.target.offsetTop
    }
    
    reDrawingOnHold(draw) {
        const img = new Image()
        img.src = this.prevDraft
        
        img.onload = () => {
            canvasState.cleanUpCanvas()
            this.ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height)
            
            draw()
        }
    }
}
