import Tool from './Tool'
import userState from '../../shared/store/userState'
import {methodMap} from '../../shared/api/WS'
import {controlMap, toolTypeMap} from '../../shared/constants/maps'

export default class SeamlessDrawing extends Tool {
    toString() {
        return toolTypeMap.SEAMLESS
    }
    
    mouseUpHandler() {
        this.mouseDown = false
        this.ctx.beginPath()
        
        if (userState.isLogged) {
            const control = {type: controlMap.FINISH}
            userState.sendMessage(methodMap.CONTROL, control)
        }
    }
    
    mouseDownHandler(e) {
        this.beforeStartDrawing()
        
        this.mouseDown = true
        
        const x = e.pageX - e.target.offsetLeft
        const y = e.pageY - e.target.offsetTop
        this.ctx.moveTo(x, y)
    }
}
