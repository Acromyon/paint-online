import PreviewDrawing from '../PreviewDrawing'
import {toolTypeMap} from '../../../shared/constants/maps'

export default class Rect extends PreviewDrawing {
    toString() {
        return toolTypeMap.RECT
    }
    
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            let currentX = e.pageX - e.target.offsetLeft
            let currentY = e.pageY - e.target.offsetTop
            this.width = currentX - this.startX
            this.height = currentY - this.startY
            
            this.reDrawingOnHold(() => {
                Rect.draw(this.ctx, this.startX, this.startY, this.width, this.height)
            })
        }
    }
    
    composeFigure() {
        return {
            type: this.toString(),
            x: this.startX,
            y: this.startY,
            w: this.width,
            h: this.height,
            styles: {
                lineWidth: this.ctx.lineWidth,
                strokeStyle: this.ctx.strokeStyle,
                fillStyle: this.ctx.fillStyle,
            }
        }
    }
    
    static draw(ctx, x, y, w, h) {
        ctx.beginPath()
        ctx.rect(x, y, w, h)
        ctx.fill()
        ctx.stroke()
        ctx.beginPath()
    }
}
