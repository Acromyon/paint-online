import Brush from './Brush'
import userState from '../../../shared/store/userState'
import {methodMap} from '../../../shared/api/WS'
import {toolTypeMap} from '../../../shared/constants/maps'

export default class Eraser extends Brush {
    toString() {
        return toolTypeMap.ERASER
    }
    
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            this.x = e.pageX - e.target.offsetLeft
            this.y = e.pageY - e.target.offsetTop
            
            Eraser.draw(this.ctx, this.x, this.y)
            
            if (userState.isLogged) {
                const figure = this.composeFigure()
                userState.sendMessage(methodMap.DRAW, figure)
            }
        }
    }
    
    static draw(ctx, x, y) {
        this.currentColor = ctx.strokeStyle
        ctx.strokeStyle = 'white'
        
        ctx.lineTo(x, y)
        ctx.stroke()
        
        ctx.strokeStyle = this.currentColor
    }
}
