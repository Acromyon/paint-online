import SeamlessDrawing from '../SeamlessDrawing'
import userState from '../../../shared/store/userState'
import {methodMap} from '../../../shared/api/WS'
import {toolTypeMap} from '../../../shared/constants/maps'

export default class Brush extends SeamlessDrawing {
    toString() {
        return toolTypeMap.BRUSH
    }
    
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            this.x = e.pageX - e.target.offsetLeft
            this.y = e.pageY - e.target.offsetTop
            
            Brush.draw(this.ctx, this.x, this.y)
            
            if (userState.isLogged) {
                const figure = this.composeFigure()
                userState.sendMessage(methodMap.DRAW, figure)
            }
        }
    }
    
    composeFigure() {
        return {
            type: this.toString(),
            x: this.x,
            y: this.y,
            styles: {
                lineWidth: this.ctx.lineWidth,
                strokeStyle: this.ctx.strokeStyle,
            }
        }
    }
    
    static draw(ctx, x, y) {
        ctx.lineTo(x, y)
        ctx.stroke()
    }
}
