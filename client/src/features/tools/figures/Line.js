import PreviewDrawing from '../PreviewDrawing'
import {toolTypeMap} from '../../../shared/constants/maps'

export default class Line extends PreviewDrawing {
    toString() {
        return toolTypeMap.LINE
    }
    
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            this.currentX = e.pageX - e.target.offsetLeft
            this.currentY = e.pageY - e.target.offsetTop
            
            this.reDrawingOnHold(() => {
                Line.draw(this.ctx, this.startX, this.startY, this.currentX, this.currentY)
            })
        }
    }
    
    composeFigure() {
        return {
            type: this.toString(),
            startX: this.startX,
            startY: this.startY,
            x: this.currentX,
            y: this.currentY,
            styles: {
                lineWidth: this.ctx.lineWidth,
                strokeStyle: this.ctx.strokeStyle,
            }
        }
    }
    
    static draw(ctx, startX, startY, x, y) {
        ctx.beginPath()
        ctx.moveTo(startX, startY)
        ctx.lineTo(x, y)
        ctx.stroke()
        ctx.beginPath()
    }
}
