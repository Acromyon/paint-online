import PreviewDrawing from '../PreviewDrawing'
import {toolTypeMap} from '../../../shared/constants/maps'

export default class Circle extends PreviewDrawing {
    toString() {
        return toolTypeMap.CIRCLE
    }
    
    mouseMoveHandler(e) {
        if (this.mouseDown) {
            let currentX = e.pageX - e.target.offsetLeft
            let currentY = e.pageY - e.target.offsetTop
            let width = Math.abs(currentX - this.startX)
            let height = Math.abs(currentY - this.startY)
            this.radius = width > height ? width : height
            
            this.reDrawingOnHold(() => {
                Circle.draw(this.ctx, this.startX, this.startY, this.radius)
            })
        }
    }
    
    composeFigure() {
        return {
            type: this.toString(),
            x: this.startX,
            y: this.startY,
            radius: this.radius,
            styles: {
                lineWidth: this.ctx.lineWidth,
                strokeStyle: this.ctx.strokeStyle,
                fillStyle: this.ctx.fillStyle,
            }
        }
    }
    
    static draw(ctx, x, y, radius) {
        ctx.beginPath()
        ctx.arc(x, y, radius, 0, 2 * Math.PI)
        ctx.fill()
        ctx.stroke()
        ctx.beginPath()
    }
}
