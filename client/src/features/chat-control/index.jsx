import React, {useRef, useState} from 'react'

import userState from '../../shared/store/userState'
import {methodMap} from '../../shared/api/WS'
import {shortcutMap} from '../../app/keyboardControl'
import TextArea from '../../shared/UI/textarea__uncontrollable'
import Button from '../../shared/UI/button'

import styles from './styles.module.scss'
import {checkedPressKeyWithModify} from '../../shared/utils/keyboardControl'

const ChatControl = () => {
    const textAreaRef = useRef(null)
    const [isFilled, setIsFilled] = useState(false)
    
    const sendMessage = () => {
        if (!isFilled) return
        
        userState.sendMessage(methodMap.TEXT, composeMessage())
        
        textAreaRef.current.innerText = ''
        textAreaRef.current.focus()
        setIsFilled(false)
    }
    
    const composeMessage = () => ({
        username: userState.username,
        text: textAreaRef.current.innerText.trim(),
        date: new Date().toJSON()
    })
    
    return (
        <>
            <TextArea
                className={styles.textarea}
                textAreaRef={textAreaRef}
                onKeyPress={(e) => checkedPressKeyWithModify(e, 'Enter', 'ctrlKey', sendMessage)}
                onInput={(e) => setIsFilled(Boolean(e.target.innerText))}
            />
            <Button
                type="primary"
                onClick={sendMessage}
                title={shortcutMap.sendChatMsg}
                disabled={!isFilled}
            >
                Send message
            </Button>
        </>
    )
}

export default ChatControl
