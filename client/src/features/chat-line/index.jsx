import React from 'react'
import {observer} from 'mobx-react-lite'

import chatState from '../../shared/store/chatState'
import {Text} from '../../shared/UI/typography'

import styles from './styles.module.scss'

const ChatLine = observer(() => {
    return (
        <div className={styles.chatLineFrame}>
            <div className={styles.chatLine}>
                {chatState.chatLine.map((msgItem) => {
                    const key = msgItem.date.slice(-10) + msgItem.username.slice(-6)
                    
                    return (
                        <Text fz={16} key={key} className={styles.message}>
                            <strong>{msgItem.username}</strong>:
                            <br/>
                            {msgItem.text}
                        </Text>
                    )
                })}
            </div>
        </div>
    )
})

export default ChatLine
