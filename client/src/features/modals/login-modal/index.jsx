import React, {useState} from 'react'
import {useNavigate, useParams} from 'react-router-dom'
import {observer} from 'mobx-react-lite'

import Modal from '../../../shared/UI/modal'
import Button from '../../../shared/UI/button'
import {Text, Title} from '../../../shared/UI/typography'
import Input from '../../../shared/UI/input'
import userState from '../../../shared/store/userState'
import {checkPressedKey} from '../../../shared/utils/keyboardControl'

import styles from '../styles.module.scss'

const LoginModal = observer(() => {
    const {roomId} = useParams()
    const [username, setUsername] = useState('')
    const navigate = useNavigate()
    
    const close = () => navigate('/')
    const login = () => {
        if (username.length) {
            userState.connectToLobby(roomId, username)
            userState.setIsLogged(true)
        }
    }
    
    const titleText = 'Введите имя чтобы зайти в лобби:'
    const text = '*отмена чтобы создать своё лобби или рисовать offline'
    
    return (
        <Modal
            onKeyPress={e => checkPressedKey(e, 'Enter', login)}
            close={close}
            className={styles.modalContainer}
        >
            <Title className={styles.title}>{titleText}</Title>
            <div>
                <Input
                    type="text"
                    placeholder="Никнейм"
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                />
                <Text fz="16" fw="light" className={styles.text}>{text}</Text>
            </div>
            <div className={styles.btnContainer}>
                <Button
                    onClick={login}
                    type="primary"
                    disabled={!username.length}
                >
                    Войти
                </Button>
                <Button onClick={close} type="close">Отмена</Button>
            </div>
        </Modal>
    )
})

export default LoginModal
