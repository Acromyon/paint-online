import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {observer} from 'mobx-react-lite'

import appState from '../../../shared/store/appState'
import userState from '../../../shared/store/userState'
import Modal from '../../../shared/UI/modal'
import Button from '../../../shared/UI/button'
import {Text, Title} from '../../../shared/UI/typography'
import Input from '../../../shared/UI/input'
import {hexadecimalStr} from '../../../shared/utils/createUniqueString'
import {checkPressedKey} from '../../../shared/utils/keyboardControl'

import styles from '../styles.module.scss'

const LobbyModal = observer(() => {
    const [username, setUsername] = useState('')
    const roomId = hexadecimalStr()
    const navigate = useNavigate()
    
    const close = () => appState.setIsOpenLobbyModal(false)
    const login = () => {
        if (username.length) {
            userState.connectToLobby(roomId, username)
            userState.setIsLogged(true)
            navigate(`/lobby/${roomId}`)
        }
    }
    
    const titleText = 'Введите имя чтобы создать лобби и рисовать online:'
    const text = '*затем передайте сгенерированный URL вашим друзьями'
    
    return (
        <Modal
            onKeyPress={e => checkPressedKey(e, 'Enter', login)}
            close={close}
            className={styles.modalContainer}
        >
            <Title className={styles.title}>{titleText}</Title>
            <div>
                <Input
                    type="text"
                    placeholder="Никнейм"
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                />
                <Text fz="16" fw="light" className={styles.text}>{text}</Text>
            </div>
            <div className={styles.btnContainer}>
                <Button
                    onClick={login}
                    type="primary"
                    disabled={!username.length}
                >
                    Войти
                </Button>
                <Button onClick={close} type="close">Offline</Button>
            </div>
        </Modal>
    )
})

export default LobbyModal
