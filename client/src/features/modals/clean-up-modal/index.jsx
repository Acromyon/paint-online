import React from 'react'
import {observer} from 'mobx-react-lite'

import Modal from '../../../shared/UI/modal'
import Button from '../../../shared/UI/button'
import {Title} from '../../../shared/UI/typography'
import {checkPressedKey} from '../../../shared/utils/keyboardControl'
import appState from '../../../shared/store/appState'
import canvasState from '../../../shared/store/canvasState'
import userState from '../../../shared/store/userState'
import {methodMap} from '../../../shared/api/WS'

import styles from '../styles.module.scss'
import ownStyles from './styles.module.scss'
import {controlMap} from '../../../shared/constants/maps'

const CleanUpModal = observer(() => {
    const close = () => appState.setIsOpenCleanUpModal(false)
    const cleanUp = () => {
        canvasState.pushToUndo()
        canvasState.clearRedo()
        
        canvasState.cleanUpCanvas()
        appState.setIsOpenCleanUpModal(false)
        
        if (userState.isLogged) {
            const startControl = {type: controlMap.START}
            const cleanUpControl = {type: controlMap.CLEANUP}
            
            userState.sendMessage(methodMap.CONTROL, startControl)
            userState.sendMessage(methodMap.CONTROL, cleanUpControl)
        }
    }
    
    const titleText = 'Вы уверенны, что хотите очистить холст?'
    
    return (
        <Modal
            onKeyPress={e => checkPressedKey(e, 'Enter', cleanUp)}
            close={close}
            className={ownStyles.modalContainer}
        >
            <Title tagType='h5' className={styles.title}>{titleText}</Title>
            <div className={styles.btnContainer}>
                <Button
                    onClick={cleanUp}
                    type="primary"
                >
                    Очистить
                </Button>
                <Button onClick={close} type="close">Отмена</Button>
            </div>
        </Modal>
    )
})

export default CleanUpModal
