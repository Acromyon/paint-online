import React from 'react'
import ReactDOM from 'react-dom/client'

import App from './app'
import reportWebVitals from './shared/utils/reportWebVitals'

import './shared/styles/common/reset.scss'
import './shared/styles/common/basic.scss'
import './shared/styles/common/titles.scss'

const root = ReactDOM.createRoot(document.getElementById('root'))

root.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>
)

reportWebVitals()
