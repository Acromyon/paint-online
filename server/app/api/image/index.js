const express = require('express')
const router = express.Router();
const fs = require('fs')
const path = require('path')

router.get('/', (req, res) => {
    try {
        fs.readFile(path.resolve(process.cwd(), 'files/images', `${req.query.id}.png`), (err, img) => {
            if (img) {
                const data = 'data:image/png;base64,' + img.toString('base64')
                return res.json(data)
            }
            return res.send(`image not found or doesn't exit`)
        })
    } catch (err) {
        console.log(err)
        
        return res.status(500).json('server error')
    }
})

router.post('/', (req, res) => {
    try {
        const img = req.body.img
            .replace('data:image/png;base64,', '')
        
        fs.writeFile(path.resolve(process.cwd(), 'files/images', `${req.query.id}.png`), img, 'base64', () => {
            return res.status(200).json({image: 'file downloaded'})
        })
    } catch (err) {
        console.log(err)
        
        return res.status(500).json('server error')
    }
})

module.exports = router
