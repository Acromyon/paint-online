const express = require('express')
const app = express()
const cors = require('cors')
const WSServer = require('express-ws')(app)
const aWss = WSServer.getWss()
const image = require('./api/image')
const {
    connectionHandler,
    disconnectHandler,
    textHandler,
    drawHandler,
    controlHandler,
    methodMap,
} = require('./controllers/WSController')

require('dotenv').config()
const PORT = process.env.PORT || 5000

app.use(cors())
app.use(express.json())

app.use('/api/image', image)

app.ws('/', (ws) => {
    ws.on('message', msg => {
        try {
            const data = JSON.parse(msg)
        
            switch (data.method) {
                case methodMap.CONNECTION:
                    connectionHandler(data, aWss, ws)
                    break
                case methodMap.TEXT:
                    textHandler(data, aWss)
                    break
                case methodMap.DRAW:
                    drawHandler(data, aWss)
                    break
                case methodMap.CONTROL:
                    controlHandler(data, aWss)
                    break
            }
        } catch (err) {
            console.log(err)
        }
    })
    ws.on('close', (code, msg) => {
        try {
            const data = JSON.parse(msg)
            disconnectHandler(data, aWss)
        } catch (err) {
            console.log(err)
        }
    })
    ws.on('error', err => console.log(err))
})

app.get('/', (req, res) => {
    try {
        res.send('Express is started');
    } catch (err) {
        console.log(err)
        
        return res.status(500).json('server error')
    }
})

app.listen(PORT, () => console.log(`server started on PORT ${PORT}`))
