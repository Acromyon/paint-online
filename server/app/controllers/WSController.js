exports.connectionHandler = (data, aWss, ws) => {
    ws.roomId = data.roomId
    ws.userId = data.userId
    
    const itselfText =
`connection established:
name \"${data.username}\", id \"${data.userId}\"`
    
    const itselfMsg = formServerMessage(this.methodMap.TEXT, itselfText)
    ws.send(itselfMsg)
    
    const broadcastText = `user \"${data.username}\" connected`
    const broadcastMsg = formServerMessage(this.methodMap.TEXT, broadcastText)
    broadcast(broadcastMsg, data, aWss, toOtherUsersInRoom)
}

exports.disconnectHandler = (data, aWss) => {
    const text = `user \"${data.username}\" disconnected by himself`
    
    const msg = formServerMessage(this.methodMap.TEXT, text)
    broadcast(msg, data, aWss, toAllUsersInRoom)
}

exports.textHandler = (data, aWss) => {
    const msg = formMessage(this.methodMap.TEXT, data.payload)
    broadcast(msg, data, aWss, toAllUsersInRoom)
}

exports.drawHandler = (data, aWss) => {
    const msg = formMessage(this.methodMap.DRAW, data.payload)
    broadcast(msg, data, aWss, toOtherUsersInRoom)
}

exports.controlHandler = (data, aWss) => {
    const msg = formMessage(this.methodMap.CONTROL, data.payload)
    broadcast(msg, data, aWss, toOtherUsersInRoom)
}

const formMessage = (method, payload, attached = {}) => JSON.stringify({
    method,
    payload,
    ...attached
})

const formServerMessage = (method, text) => JSON.stringify({
    method,
    payload: {
        username: 'WS-server',
        text,
        date: new Date().toJSON()
    }
})

const broadcast = (msg, data, aWss, condition) => {
    aWss.clients.forEach(client => {
        if (condition ? condition(client, data) : true) {
            client.send(msg)
        }
    })
}

const toOtherUsersInRoom = (client, data) => client.roomId === data.roomId && client.userId !== data.userId
const toAllUsersInRoom = (client, data) => client.roomId === data.roomId

exports.methodMap = {
    TEXT: 'text',
    DRAW: 'draw',
    CONTROL: 'control',
    CONNECTION: 'connection'
}
